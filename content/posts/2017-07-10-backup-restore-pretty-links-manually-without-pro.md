---
redirect_canonical: true
redirect_to: https://cspeterson.net/backup-restore-pretty-links-manually-without-pro
layout: post
title: Backup and restore Pretty Links manually on Wordpress (without Pro)
date: 2017-07-10 23:01:11.000000000 -04:00
type: post
parent_id: '0'
published: true
password: ''
status: publish
categories:
  - blog
tags:
  - mysql
  - Pretty Links
  - WordPress
author: Christopher Peterson
slug: 'backup-restore-pretty-links-manually-without-pro'

---
I needed to move my Pretty Link data from one install to another, and plugin settings do not come along for a normal [WordPress export](https://en.support.wordpress.com/export/) so it needed to be done explicitly. Pretty Link Pro (a worthy [purchase](https://prettylinkpro.com/pricing/plans/) if you do much pretty-linking) is needed to do it via GUI the easy way, and I only have the pedestrian Free version.

So let's see what we can do the hard way. You'll need ssh access to the server and the WordPress database name, user, and password (find-able in your wp-config.php).

# Exporting

```sh
~$ mysql -u wordpressdbuser -p wordpressdbname
Enter password:
Welcome to the MySQL monitor. Commands end with ; or \g.
[...]
mysql>
```

For a single-blog normal WordPress install Pretty Link data is stored in the following tables:

```mysql
mysql> show tables;
+-----------------------------+
| Tables_in_wordpressdbname |
+-----------------------------+
[...]
| wp_prli_clicks |
| wp_prli_groups |
| wp_prli_link_metas |
| wp_prli_links |
[...]
+-----------------------
48 rows in set (0.00 sec)
```


Else on a multisite WordPress install it'll be more like this, with whatever numbers correspond to the blog on the network that you're dealing with.

```mysql
mysql> show tables;
+-----------------------------+
| Tables_in_wordpressdbname |
+-----------------------------+
[...]
| wp_4_prli_clicks |
| wp_4_prli_groups |
| wp_4_prli_link_metas |
| wp_4_prli_links |
[...]
+-----------------------
96 rows in set (0.00 sec)
```

And after taking a look at the data within, it's really what you'd expect: a list of URLs to translate into other URLs. Easy peasy, let's export the explicit list of `prli` table names (changing as appropriate for Multisite)

```sh
$ mysqldump -u mysqluser -p wordpressdbname wp_prli_clicks wp_prli_groups _p_prli_link_metas wp_prli_links > prlidump.mysql
```


Now is the time to open up Vim and change some things if necessary. If doing migration between single wordpress installs, the tables names can stay the same. If you're moving to another install of the same domain name, those can remain too. If going from normal to multisite, or from differently-numbered multisite blogs, you'll need to edit the table names. If changing domains you'll probably want to replace occurrences of the old domain name in all of the urls, or whatever. Up to you.

Then you can import.

# Importing

1. back up your DB, dummy.
2. do something like the following:

```sh
$ mysql -u wordpressdbuser -p wordpressdbname > bprlidump.mysql
```


That's all. Now you have your pretty links back. 8-)
