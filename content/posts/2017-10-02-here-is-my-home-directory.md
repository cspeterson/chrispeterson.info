---
redirect_canonical: true
redirect_to: https://cspeterson.net/here-is-my-home-directory
layout: post
title: Here is my home directory
date: 2017-10-02 11:15:37.000000000 -04:00
cover-img: /img/dotfiles.jpg
categories:
  - blog
tags:
  - configuration
  - git
  - github
  - linux
author: Christopher Peterson
slug: 'here-is-my-home-directory'
---
...or the important parts, at least.

I've finally taken the plunge and spent a morning cleaning up and organizing my [dotfiles](https://en.wikipedia.org/wiki/Dotfiles) so I can put the whole thing on GitHub and keep the same configuration across all of my home directories while sharing things and maybe making the world a tiny bit better.

[https://github.com/cspeterson/dotfiles](https://github.com/cspeterson/dotfiles)

You as a person configuring things on Linux might see something useful in there. I use it across Ubuntu and CentOS systems without problems to configure the likes of Vim, Bash, Audacity, GNU Screen, Terminator, and maybe other things. It will continue to change over time as I alter what things I use and how I use them.
